/**
 * SuiteScript Version 1.0
 * cp_ep_po_approvals
 * 
 * Date				Author				Version				Notes
 * 8/6/2019			Matt Wertheim		1.0					Script created
 * 
 * 
 */

/**
 * @desc function to process incoming email for Purchase Order approval workflow
 * @param {Object} email 
 * @returns {Void}
 */
function process(email){
	var fromEmail = email.getFrom()
	,	subject = email.getSubject()
	,	body = email.getTextBody()
	,	button = null
	,	trx = getTransaction(subject);
	
	nlapiLogExecution('DEBUG', 'email received', 'from: ' + fromEmail + ' subject: ' + subject);
	
	if(trx && fromEmail == trx.nextApprover){
		
		if(body.toLowerCase().search('#approve') > -1){
			nlapiLogExecution('DEBUG', 'approve block');
			clickButton('approve', trx.approvalLevel, trx.id);
			return;
		}
		else if(body.toLowerCase().search('#reject') > -1){
			nlapiLogExecution('DEBUG', 'approve block');
			clickButton('reject', trx.approvalLevel, trx.id);
			return;
		}
		
		
		nlapiLogExecution('ERROR', 'Invalid email body', 'Could not find approve or reject command in email.');
		
	}
}