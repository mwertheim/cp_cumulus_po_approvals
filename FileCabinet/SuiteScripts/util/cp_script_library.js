/**
 * Script library for Caravel Partners purchasing scripts
 */

/**
 * function used to get information about the transaction being approved
 * @param docnum 
 * @returns {Object}
 */
function getTransaction(docnum){
	if(docnum!='' && docnum != null){
		nlapiLogExecution('DEBUG', 'docnum', docnum);
		try{
			var filters = [
			               new nlobjSearchFilter('tranid', null, 'startswith', docnum)
			               ];
			var columns = [
			               new nlobjSearchColumn('internalid'),
			               new nlobjSearchColumn('custbody_cp_approval_level'),
			               new nlobjSearchColumn('email', 'nextapprover')
			               ];
		
			var results = nlapiSearchRecord('purchaseorder', null, filters, columns);
			
			if(results && results.length > 0){
				return {
					id: results[0].id,
					approvalLevel: results[0].getValue('custbody_cp_approval_level'),
					nextApprover: results[0].getValue('email', 'nextapprover')
				};
			}
			else{
				nlapiLogExecution('AUDIT', 'Could not find transaction', 'Document Number: ' + docnum);
				return null;
			}
		}
		catch(e){
			nlapiLogExecution('ERROR', 'Error finding transaction', e);
		}
		
	}
}

/**
 * Function used to simulate a workflow button click
 * @param button
 * @param level
 * @param trxId
 * @returns {Void}
 */
function clickButton(button, level, trxId){
	var stateId = null;
	nlapiLogExecution('DEBUG', 'clickButton approval level', level);
	
	switch(level){
	case 'supervisor': 
		stateId = 'workflowstate_cp_po_supervisor_approval';
		break;
	case 'dept_head':
		stateId = 'workflowstatecp_po_dept_head_approval';
		break;
	case 'cfo':
		stateId = 'workflowstatecp_po_cfo_approval';
		break;
		default:
			nlapiLogExecution('ERROR', 'No Approval Level Found');
			return;
	}
	
	if(stateId != null){
		try{
			var buttonId = 'workflowaction_cp_email_' + button;
			nlapiLogExecution('DEBUG', 'Clicking...', 'trxId = ' + trxId + ' button = ' + buttonId + ' state = ' + stateId);
			nlapiTriggerWorkflow('purchaseorder', trxId, 'customworkflow_cp_po_approvals', buttonId, stateId);
		}
		catch(e){
			nlapiLogExecution('ERROR', 'Order Approval', e);
		}
	}
}
